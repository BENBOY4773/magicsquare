package edu.ecpi.benboy4773.magicsquare;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * @author      Ben Boyle 			<benboy4773@students.ecpi.edu>
 * @version     1.0                 (current version number of program)
 * @since       2014-02-06          (the version of the package this class was first added to)
 */
public class Main {
    
	private static int getDimension(){
    	System.out.print("How wide is your square (odd number required)? ");
    	int num;
    	Scanner in = new Scanner(System.in);

        try {
          num = in.nextInt();
        }
    	catch (InputMismatchException e) {
    		System.out.println("Your entry must be numeric");
    		num = getDimension();//recursive, but ought to work
    	}
        		
        in.close();
        
    	return(num);
    }
	
	public static void main(String[] args) {
		int dimension;

		try {
			dimension = getDimension();
			MagicSquare board = new MagicSquare(dimension);
			System.out.println("Magic Square of dimensions " + board.getXDimension() + "X" + board.getYDimension() + ":\n");
			board.printSquare();

			System.out.println("");
			System.out.println("Dr Patrick's variant:");
			System.out.println("");
			MagicSquarePatrick square = new MagicSquarePatrick();
			square.setN(dimension);
			System.out.println(square);

		
		
		}
		catch (IllegalArgumentException e) {
    		System.out.println(e.getMessage());
			
		}

		

	}

}
