package edu.ecpi.benboy4773.magicsquare;

/**
 * Based on Dr. G. Ptrick's class example after review
 * @author Ben
 *
 */
public class MagicSquarePatrick {
	protected int[][] square;
	protected int n;
	protected int currentRow;
	protected int currentCol;
	
	public void setN(int n){
		if (n % 2 == 1){
			this.n = n;
		}
		else {				
			this.n = n;
		}
		computeSquare();
	}
	
	public int getN(){
		return this.n;
	}
	
	public String toString(){
		StringBuffer result = new StringBuffer("");
		int colWidth = Integer.toString(n*n).length()+ 2; //Add two to allow for parentheses  
		
		for (int row = 0; row < n; row++){
			  for  (int col = 0; col < n; col++){
				  result.append(String.format("%1$"+colWidth+"s", this.square[row][col])+ " ");
			  }
			  result.append("\n");
		  }
		return result.toString();
	}
	
	private void computeSquare(){
		square = initializeSquare(n);
		currentCol = n/2;
		square[0][currentCol] = 1;
		
		int nSquared = n*n;//calculate once, rather than per iteration
		for ( int i = 2 ; i <= nSquared ; i++){
			
			int nextRow = subOne(currentRow);
			int nextCol = addOne(currentCol);
			
			if (square[nextRow][nextCol] == 0) {
				square[nextRow][nextCol] =  i;
			}
			else {
				nextRow = addOne(currentRow);
				nextCol = currentCol;
				square[nextRow][nextCol] = i;
			}
			currentRow = nextRow;
			currentCol = nextCol;
		}
	}
	
	private int addOne(int value){
		return (value + 1) % n;
	}
	
	private int subOne(int value){
		//go FORWARD around the loop to avoid negatives
		return (value + n -1) % n;
	}
	
	private int[][] initializeSquare(int size){
		return new int[size][size];
	}
}
