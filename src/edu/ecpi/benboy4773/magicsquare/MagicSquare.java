package edu.ecpi.benboy4773.magicsquare;

/**
 * @author      Ben Boyle 			<benboy4773@students.ecpi.edu>
 * @version     1.0                 (current version number of program)
 * @since       2014-02-06          (the version of the package this class was first added to)
 */
public class MagicSquare { 
  private int[][] board;
  private int[] sumHorizontal;
  private int[] sumVertical;
  private int sumDiagonal;
  

  protected void populateSquare(){
	  int boardSize = this.getXDimension();
	  int xCounter = this.getXDimension() / 2;
	  int yCounter = this.getBoardVolume();
	  
	  
	  int xCoord, yCoord;
	  
	  //for (int i = 1; i <= 6; i++){
	  for (int i = 1; i <= this.getBoardVolume(); i++){
		  xCoord = xCounter % boardSize;
		  yCoord = yCounter % boardSize;
		  
		  if (this.board[xCoord][yCoord] != 0){
			  yCounter = yCounter + 2;// jump down TWO rows		
			  xCounter--;// jump left one column		
			  xCoord = xCounter % boardSize;
			  yCoord = yCounter % boardSize;
		  }
		  setSquareValue(xCoord, yCoord, i);
		  
		  xCounter++;
		  yCounter--;
		  
		  //Uncomment this line to see the square at each step
		  //this.printSquare();
	  }
  }

  private void setSquareValue(int xCoord, int yCoord, int value) {
	  this.board[xCoord][yCoord] = value;
	  this.sumHorizontal[yCoord] += value;
	  this.sumVertical[xCoord] += value;
	  if (xCoord == yCoord){
		  this.sumDiagonal += value;
	  }
  }
  
  protected void setDimension(int dimension){
	  if (dimension % 2 == 0) {
		  throw new IllegalArgumentException("Dimensions must be an odd number.");
	  }
	  this.board = new int [dimension][dimension];
	  this.sumHorizontal = new int[dimension];
	  this.sumVertical = new int[dimension];
	  this.sumDiagonal = 0;
  }

  public MagicSquare(int dimension){
	  this.setDimension(dimension);
	  this.populateSquare();
  }

  public void printSquare(){
	  //how much space do we need for our largest number?
	  //Use the diagonal as the sums will be the largest and pulling an int is easier 
	  //than grabbing the first item in an array
	  int colWidth = Integer.toString(this.sumDiagonal).length()+ 2; //Add two to allow for parentheses  
	  
	  for (int y = 0; y < getYDimension(); y++){
		  for  (int x = 0; x < getXDimension(); x++){
			  System.out.print(String.format("%1$"+colWidth+"s", this.board[x][y])+ " ");
		  }
		  System.out.print(" (" + this.sumHorizontal[y] + ")");
		  System.out.println("");
	  }
	  //Column totals
	  for  (int x = 0; x < getXDimension(); x++){
		  System.out.print(String.format("%1$"+colWidth+"s", " (" + this.sumVertical[x] + ")") + "");
	  }
	  System.out.print(" (" + this.sumDiagonal + ")");
	  
	  //blank line
	  System.out.println("");
  }

  public int getYDimension() {
	  return this.board[0].length;
  }

  public int getXDimension() {
	  return this.board.length;
  }
  
  protected int getBoardVolume(){
	  return getXDimension() * getYDimension(); 
  }
  
}
